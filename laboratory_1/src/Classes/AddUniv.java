package Classes;

import java.util.List;

public class AddUniv {
    public String university;
    public List<String> students;
    public List<String> marks;

     public void createUniversity(String name, List<String> students, List<String> marks) {
        this.university = name;
        this.students = students;
        this.marks = marks;
        System.out.println(name);
        System.out.println(students);
        System.out.println(marks);
    }
}