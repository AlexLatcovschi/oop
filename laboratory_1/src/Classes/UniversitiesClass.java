package Classes;

import java.util.List;

 public class UniversitiesClass {
     public String name;
     public int foundationYear;
     public List<String> students;

     public void addUniversity(String name, int foundationYear, List<String> students) {
        this.name = name;
        this.foundationYear = foundationYear;
        this.students = students;
        System.out.println(this.name);
        System.out.println(this.foundationYear);
        System.out.println(this.students);

    }

}