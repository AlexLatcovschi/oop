package TaskLevels;

import java.util.ArrayList;
import java.util.Random;
import java.util.List;
import Classes.AddUniv;
class AdvancedLevel2 {
    public static void main(String[] args) {
     displayAVG();
    }
     static void displayAVG() {
        Random rand = new Random();
        String[] universitiesArray = {"UTM", "USM", "ASEM"};
        String[] student = {"Kr", "Ca", "Ra", "Mrok", "Cru7",
                "Ray", "Bre", "Zed", "Drak", "Mor", "Jag", "Mer", "Jar", "Mjol",
                "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro",
                "Mar", "Luk"};
        AddUniv univ = new AddUniv();
        List<String> allMarks = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            var localUniv = universitiesArray[i];
            List<String> studList = new ArrayList<>();
            List<String> studMarks = new ArrayList<>();

            for (int j = 0; j < (int) ((Math.random() * ((200 - 100) + 1)) + 100); j++) {
                String students = student[rand.nextInt(student.length)];
                studList.add(students);
                double mark = (Math.random() * ((10 - 5) + 1)) + 5;
                while(mark > 10) {
                    mark = (Math.random() * ((10 - 5) + 1)) + 5;
                }
                String markS = Double.toString(mark);
                studMarks.add(markS);
            }
            allMarks.addAll(studMarks);
            univ.createUniversity(localUniv, studList, studMarks);
        }
        double[] marksArray = new double[allMarks.size()];

        double averageMark = 0;
        for (int i = 0; i < allMarks.size(); i++) {
            marksArray[i] = Double.parseDouble(allMarks.get(i));
            averageMark = averageMark + marksArray[i];
            if (i + 1 == allMarks.size()) {
                averageMark = averageMark / allMarks.size();
                System.out.println(averageMark);
            }
        }
    }
}
