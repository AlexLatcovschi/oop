package TaskLevels;

import Classes.Student;
import Classes.UniversitiesClass;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AdvancedLevel {
    public static void main(String[] args) {
        addUnivInfo();
    }

    static void addUnivInfo() {
        String[] student = {"Kr", "Ca", "Ra", "Mrok", "Cru",
                "Ray", "Bre", "Zed", "Drak", "Mor", "Jag", "Mer", "Jar", "Mjol",
                "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro",
                "Mar", "Luk"};
        String[] universities = {"UTM", "USM", "ASEM", "ULIM", "Ion Creanga"};
        Random rand = new Random();
        Student studentClass = new Student();
        UniversitiesClass univClass = new UniversitiesClass();
        int studNum = (int) (Math.random() * ((2000 - 400) + 1)) + 400;
        List<String> studList = new ArrayList<>();
        List<String> list1;
        List<String> list2;
        List<String> list3;
        List<String> list4;
        List<String> list5;

        for (int i = 0; i < studNum; i++) {
            String students = student[rand.nextInt(student.length)];
            int age = (int) (Math.random() * ((50 - 18) + 1)) + 18;
            double mark = (Math.random() * ((10 - 5) + 1)) + 5;
            while (mark > 10) {
                mark = (int) (Math.random() * ((4) + 1));
            }

            studList.add(students);
            studentClass.addStudent(students, age, mark);
        }
        list1 = studList.subList(0, studNum / 5);
        list2 = studList.subList(studNum / 5, studNum * 2 / 5);
        list3 = studList.subList(studNum * 2 / 5, studNum * 3 / 5);
        list4 = studList.subList(studNum * 3 / 5, studNum * 4 / 5);
        list5 = studList.subList(studNum * 4 / 5, studNum * 5 / 5);

        for (int i = 0; i < universities.length; i++) {
            var foundationYear = (int) ((Math.random() * ((1900 - 2019) + 1)) + 2000);
            while (foundationYear > 2019) {
                foundationYear = (int) (Math.random() * ((4) + 1));
            }
            if (i == 0) {
                univClass.addUniversity(universities[i], foundationYear, list1);
            } else if (i == 1) {
                univClass.addUniversity(universities[i], foundationYear, list2);
            }
            if (i == 2) {
                univClass.addUniversity(universities[i], foundationYear, list3);
            }
            if (i == 3) {
                univClass.addUniversity(universities[i], foundationYear, list4);
            }
            if (i == 4) {
                univClass.addUniversity(universities[i], foundationYear, list5);
            }
        }
    }

}
