package Advanced_Level;

public class QueueManipulating {
    public static void main(String[] args) {
        Queue Queue1 = new Queue();
        Queue Queue2 = new Queue(3);

        Queue1.q.add(22);
        Queue1.q.add(31);
        Queue1.q.add(45);

        System.out.println(Queue1.q);
        Queue1.CheckQueue();

        System.out.println("\n");

        Queue2.q.add(32);
        Queue2.q.add(13);
        Queue2.q.add(1111);
        Queue2.q.add(23);
        Queue2.q.add(999);

        System.out.println(Queue2.q);
        Queue2.CheckQueue();

    }

}
