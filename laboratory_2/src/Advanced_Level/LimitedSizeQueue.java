package Advanced_Level;
import java.util.LinkedList;

    public class LimitedSizeQueue<K> extends LinkedList<K> {
        private int maxSize;

        public LimitedSizeQueue(int size) {
            this.maxSize = size;
        }

        public boolean add(K k) {
            boolean r = super.add(k);
            if (size() > maxSize) {
                removeRange(0, size() - maxSize);
            }
            return r;
        }
    }
