package Advanced_Level;
import java.util.LinkedList;

public class Queue {
    private int capacity = -1;
    public java.util.Queue<Integer> q = new LinkedList<Integer>();

    public Queue() {
    }

    public Queue(int maximumSize) {
        capacity = maximumSize;
        this.q = new LimitedSizeQueue(maximumSize);
    }

    public String CheckQueue() {
        if (capacity == -1) {
            System.out.println("Queue is never full");
            return "Queue is never full";
        } else {
            if (q.size() == capacity) {
                System.out.println("Queue is full");
                return "Queue is full";
            } else if (q.size() == 0) {
                System.out.println("Queue is empty");
                return "Queue is empty";
            } else {
                System.out.println("You can add " + (capacity - q.size()) + " elements");
                return "You can add" + (capacity - q.size()) + "elements";
            }
        }
    }
}
