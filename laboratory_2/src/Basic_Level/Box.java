package Basic_Level;

public class Box {
    public double height = 1;
    public double width = 1;
    public double depth = 1;

    public Box() {
    }
    public Box(double value) {
        this.height = value;
        this.width = value;
        this.depth = value;
    }
    public Box(double height, double width, double depth) {
        this.height = height;
        this.width = width;
        this.depth = depth;
    }
}
