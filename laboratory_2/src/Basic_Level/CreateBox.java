package Basic_Level;

public class CreateBox {
    public static void main(String[] args) {
        Box BoxDefaultValue = new Box();
        Box BoxOneParameter = new Box(3);
        Box BoxThreeParameter = new Box(3, 6, 4);
    }
}
