package Advanced_Level_2;
import Basic_Level.Box;

public class BoxMath {
    public static void main(String[] args) {
        Box box = new Box(31, 33.4, 3.333);

        double volume = BoxVolume(box.width, box.height, box.depth);
        System.out.println(volume);

        double aria = BoxAria(box.width, box.height, box.depth);
        System.out.println(aria);
    }

    static double BoxAria(double width, double height, double depth) {
        double aria = 2 * (width + height + depth);
        return aria;
    }
    static double BoxVolume(double width, double height, double depth) {
        double volume = width * height * depth;
        return volume;
    }
}
