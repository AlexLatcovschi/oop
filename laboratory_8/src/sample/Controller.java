package sample;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Controller {
    @FXML
    public TextField firstNumber;
    @FXML
    public TextField secondNumber;
    @FXML
    public Text firstRequired;
    @FXML
    public Text secondRequired;
    @FXML
    public Text justNumbersAllowed;
    @FXML
    public Text canNotBe13;

    @FXML
    protected void handleSubmitButtonAction() {
        try {
            if (firstNumber.getText().length() == 0) {
                System.out.println(1 / 0);
            }
        } catch (Exception e) {
            firstRequired.setText(new InputRequiredException().returnError("First"));
        }
        try {
            if (secondNumber.getText().length() == 0) {
                System.out.println(1 / 0);
            }
        } catch (Exception e) {
            secondRequired.setText(new InputRequiredException().returnError("Second"));
        }
        try {
            if (secondNumber.getText().equals("13")) {
                System.out.println(1 / 0);
            }
        } catch (Exception e) {
            canNotBe13.setText(new UnluckyException().returnError());
        }

        String myText1 = firstNumber.getText();
        for (int i = 0; i < myText1.length(); i++) {
            if (!Character.isDigit(myText1.charAt(i))) {
                myText1 = myText1.replace(myText1.charAt(i), Character.MIN_VALUE);
            }
        }
        firstNumber.setText(myText1);

        String myText2 = secondNumber.getText();
        for (int i = 0; i < myText2.length(); i++) {
            if (!Character.isDigit(myText2.charAt(i))) {
                myText2 = myText2.replace(myText2.charAt(i), Character.MIN_VALUE);
            }
        }
        secondNumber.setText(myText2);

        if (firstNumber.getText().length() > 0 && secondNumber.getText().length() > 0 && !secondNumber.getText().equals("13")) {

            GridPane rootNode = new GridPane();
            Stage myStage = new Stage();
            Scene myScene = new Scene(rootNode, 300, 200);

            TextField result = new TextField();

            rootNode.add(new Label("Result:"), 0, 7);
            result.appendText(Float.toString(Float.parseFloat(firstNumber.getText()) / Float.parseFloat(secondNumber.getText())));
            rootNode.add(result, 1, 7);
            myStage.setScene(myScene);
            myStage.show();
        }
    }

    @FXML
    protected void setListeners() {
        if (firstNumber.getText().length() == 0 && secondNumber.getText().length() == 0) {
            firstNumber.textProperty().addListener((obs, oldText, newText) -> {
                firstNumber.setText(firstNumber.getText().trim());
                String myText = firstNumber.getText();
                for (int i = 0; i < myText.length(); i++) {
                    if (!Character.isDigit(myText.charAt(i))) {
                        try {
                            System.out.println(1 / 0);
                        } catch (Exception e) {
                            justNumbersAllowed.setText(new OnlyNumbersException().returnError());
                        }
                    }
                }
            });
            secondNumber.textProperty().addListener((obs, oldText, newText) -> {
                String myText = secondNumber.getText();
                for (int i = 0; i < myText.length(); i++) {
                    if (!Character.isDigit(myText.charAt(i))) {
                        try {
                            System.out.println(1 / 0);
                        } catch (Exception e) {
                            justNumbersAllowed.setText(new OnlyNumbersException().returnError());
                        }
                    }
                }
            });
        }
    }
}
