package sample;

public class OnlyNumbersException extends Exception {
    public String returnError() {
        return "You can add only numbers!";
    }
}
