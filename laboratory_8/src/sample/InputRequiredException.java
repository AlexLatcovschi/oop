package sample;

public class InputRequiredException extends Exception {
    public String returnError(String value) {
        return value + " number is required!";
    }
}
