import java.util.Date;

public class Person {
public String title;
public String givenName;
public String middleName;
public String familyName;
public FullName name;
public Date birthDate;
public Gender gender;
public Address homeAddress;
public Phone phone;
}
