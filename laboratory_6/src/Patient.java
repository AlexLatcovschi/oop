import java.util.Date;

public class Patient extends Person{
    public String id;
    public FullName name;
    public Gender gender;
    public Date birthDate;
    public Integer age;
    public Date accepted;
    public History sickness;
    public String[] prescriptions;
    public String[] allergies;
    public String[] specialReqs;
}
