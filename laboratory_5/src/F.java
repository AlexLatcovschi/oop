public class F extends E {
    protected String f;

    public F(String a, String b, String c, String d, String e, String f) {
        super(a, b, c, d, e);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
}
