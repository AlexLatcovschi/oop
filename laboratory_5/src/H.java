public class H extends G {
    protected String h;
    protected X x = new X("zzz");

    public H(String a, String b, String c, String d, String e, String f, String g, String h) {
        super(a, b, c, d, e, f, g);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
    }
}
