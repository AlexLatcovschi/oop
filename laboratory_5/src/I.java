public class I extends H {
    protected String i;

    public I(String a, String b, String c, String d, String e, String f, String g, String h, String i) {
        super(a, b, c, d, e, f, g, h);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
    }
}
