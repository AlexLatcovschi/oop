public class D extends C {
    protected String d;
    protected X x = new X("yyy");

    public D(String a, String b, String c, String d) {
        super(a, b, c);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
}
