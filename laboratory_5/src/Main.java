public class Main {
    public static void main(String[] args) {
        classManipulation();
    }
   private static void classManipulation() {
        
        A a = new A("aa");
        B b = new B("ba", "bb");
        C c = new C("ca","cb","cc");
        D d = new D("da","db","dc","dd");
        E e = new E("ea","eb","ec","ed","ee");
        F f = new F("fa","fb","fc","fd","fe","ff");
        G g = new G("ga","gb","gc","gd","ge","gf","gg");
        H h = new H("ha","hb","hc","hd","he","hf","hg","hh");
        I i = new I("ia","ib","ic","id","ie","if","ig","ih","ii");
        J j = new J("ja","jb","jc","jd","je","jf","jg","jh","ji","jj");

        System.out.println("a { \n" + "a: " + a.a + ",\n" + "x: " + a.x + ",\n" + "}");
        System.out.println("b { \n" + "a: " + b.a + ",\n" + "b: " + b.b + ",\n" + "x: " + b.x + ",\n" + "}");
        System.out.println("c { \n" + "a: " + c.a + ",\n" + "b: " + c.b + ",\n" + "c: " + c.c + ",\n" + "x: " + c.x + ",\n" + "}");
        System.out.println("d { \n" + "a: " + d.a + ",\n" + "b: " + d.b + ",\n" + "c: " + d.c + ",\n" + "d: " + d.d + ",\n" + "x: " + d.x + ",\n" + "}");
        System.out.println("e { \n" + "a: " + e.a + ",\n" + "b: " + e.b + ",\n" + "c: " + e.c + ",\n" + "d: " + e.d + ",\n" + "e: " + e.e + ",\n" + "x: " + e.x + ",\n" + "}");
        System.out.println("f { \n" + "a: " + f.a + ",\n" + "b: " + f.b + ",\n" + "c: " + f.c + ",\n" + "d: " + f.d + ",\n" + "e: " + f.e + ",\n" + "f: " + f.f + ",\n" + "x: " + f.x + ",\n" + "}");
        System.out.println("g { \n" + "a: " + g.a + ",\n" + "b: " + g.b + ",\n" + "c: " + g.c + ",\n" + "d: " + g.d + ",\n" + "e: " + g.e + ",\n" + "f: " + g.f + ",\n" + "g: " + g.g + ",\n" + "x: " + g.x + ",\n" + "}");
        System.out.println("h { \n" + "a: " + h.a + ",\n" + "b: " + h.b + ",\n" + "c: " + h.c + ",\n" + "d: " + h.d + ",\n" + "e: " + h.e + ",\n" + "f: " + h.f + ",\n" + "g: " + h.g + ",\n" + "h: " + h.h + ",\n" + "x: " + h.x + ",\n" + "}");
        System.out.println("i { \n" + "a: " + i.a + ",\n" + "b: " + i.b + ",\n" + "c: " + i.c + ",\n" + "d: " + i.d + ",\n" + "e: " + i.e + ",\n" + "f: " + i.f + ",\n" + "g: " + i.g + ",\n" + "h: " + i.h + ",\n" + "i: " + i.i + ",\n" + "x: " + i.x + ",\n" + "}");
        System.out.println("j { \n" + "a: " + j.a + ",\n" + "b: " + j.b + ",\n" + "c: " + j.c + ",\n" + "d: " + j.d + ",\n" + "e: " + j.e + ",\n" + "f: " + j.f + ",\n" + "g: " + j.g + ",\n" + "h: " + j.h + ",\n" + "i: " + j.i + ",\n" + "j: " + j.j + ",\n" + "x: " + j.x + ",\n" + "}");

        System.out.println(j.x);
    }
}

