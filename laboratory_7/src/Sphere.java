public class Sphere implements GeometricBody {
    public float radius;

    public Sphere(float radius) {
        this.radius = radius;
    }

    public float getSurface() {
        return (4 * (float) Math.PI * this.radius * this.radius);
    }

    public float getVolume() {
        return ((float) (4 / 3) * ((float) Math.PI * this.radius * this.radius * this.radius));
    }
}
