interface GeometricBody {
    public float getSurface();

    public float getVolume();
}
