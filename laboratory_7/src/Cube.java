public class Cube implements GeometricBody {
    public float vertex;

    public Cube(float vertex) {
        this.vertex = vertex;
    }

    public float getSurface() {
        return 6 * this.vertex * this.vertex;
    }

    public float getVolume() {
        return this.vertex * this.vertex * this.vertex;
    }
}
