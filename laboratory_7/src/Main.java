import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Cube cube = new Cube(3);
        Sphere sphere = new Sphere(10);
        Parallelepiped parallelepiped = new Parallelepiped(4,6,7);

        GeometricBody [] objects = new GeometricBody[3];
        objects[0] = cube;
        objects[1] = sphere;
        objects[2] = parallelepiped;

        GeometricBodyController geometricBodyController = new GeometricBodyController();
        geometricBodyController.getBiggestSurface(objects);
        System.out.println(geometricBodyController.getBiggestSurface(objects).getSurface());
        geometricBodyController.getBiggestVolume(objects);
        System.out.println(geometricBodyController.getBiggestVolume(objects).getVolume());

    }
}

