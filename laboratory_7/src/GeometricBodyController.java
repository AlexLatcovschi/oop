public class GeometricBodyController {
    public GeometricBody getBiggestVolume(GeometricBody[] geometricList) {
        GeometricBody maxVolume;
        maxVolume = geometricList[0];

        for (int i = 1; i <= geometricList.length - 1; i++) {
            if (geometricList[i].getVolume() > maxVolume.getVolume()) {
                maxVolume = geometricList[i];
            }
        }
        return maxVolume;
    }

    public GeometricBody getBiggestSurface(GeometricBody[] geometricList) {
        GeometricBody maxSurface;
        maxSurface = geometricList[0];
        for (int i = 1; i <= geometricList.length - 1; i++) {
            if (geometricList[i].getSurface() > maxSurface.getSurface()) {
                maxSurface = geometricList[i];
            }
        }
        return maxSurface;
    }
}
