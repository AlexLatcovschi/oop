public class Parallelepiped implements GeometricBody {
    public float width;
    public float height;
    public float depth;

    public Parallelepiped(float width, float height, float depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public float getSurface() {
        return 2 * width * height + 2 * height * depth + 2 * width * depth;
    }

    public float getVolume() {
        return width * height * depth;
    }
}
