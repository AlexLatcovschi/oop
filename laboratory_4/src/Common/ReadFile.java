package Common;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class ReadFile {
    public static ArrayList ReadFileExpressions(String file) throws IOException {
        String fileName = file;
        ArrayList fileExp = new ArrayList();

        try (InputStream fis = new FileInputStream(fileName);
             InputStreamReader isr = new InputStreamReader(fis,
                     StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(isr)) {
            br.lines().forEach(line -> fileExp.add(line));
        }
        return fileExp;
    }
}
