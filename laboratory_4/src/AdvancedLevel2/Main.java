package AdvancedLevel2;

import AdvancedLevel.ParenthesisManipulation;
import Common.ReadFile;

import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws IOException {
        StoreParenthesis(0);
        StoreParenthesis(1);
        StoreParenthesis(2);
    }

    static String StoreParenthesis(int index) throws IOException {
        int currentRow = index + 1;
        String fileExp = (String) ReadFile.ReadFileExpressions("src/storefiles/three_expressions.txt").get(index);
        ArrayList exp = new ArrayList();
        for (int i = 0; i <= fileExp.length() - 1; i++) {
            exp.add(fileExp.charAt(i));
        }
        if (ParenthesisManipulation.areParenthesisBalanced(exp)) {
            System.out.println("Parenthesis in row " + currentRow + " are put correct !!");
            return "Parenthesis in row " + currentRow + " are put correct !!";
        } else {
            System.out.println("Parenthesis in row " + currentRow + " are not put correct !!");
            return "Parenthesis in row " + currentRow + " are not put correct !!";
        }

    }
}

