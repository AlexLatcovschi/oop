package AdvancedLevel;

import Common.ReadFile;

import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws IOException {
        StoreParenthesis();
    }

    static int StoreParenthesis() throws IOException {
        String fileExp = (String) ReadFile.ReadFileExpressions("src/storefiles/one_expression.txt").get(0);
        ArrayList exp = new ArrayList();
        for (int i = 0; i <= fileExp.length() - 1; i++) {
            exp.add(fileExp.charAt(i));
        }
        if (ParenthesisManipulation.areParenthesisBalanced(exp)) {
            System.out.println("Parenthesis are put correct !!");
            return 1;
        } else {
            System.out.println("Parenthesis are not put correct !!");
            return 0;
        }
    }
}

