package AdvancedLevel;

import java.util.ArrayList;

public class ParenthesisManipulation {
    static boolean isMatchingPair(char character1, Object character2) {
        if (character1 == '(' && character2.equals(')'))
            return true;
        else if (character1 == '{' && character2.equals('}'))
            return true;
        else if (character1 == '[' && character2.equals(']'))
            return true;
        else
            return false;
    }
    public static boolean areParenthesisBalanced(ArrayList exp) {
        Stack st = new Stack();

        for (int i = 0; i < exp.size(); i++) {
            if (exp.get(i).equals('{') || exp.get(i).equals('(') || exp.get(i).equals('['))
                st.push((Character) exp.get(i));

            if (exp.get(i).equals('}') || exp.get(i).equals(')') || exp.get(i).equals(']')) {

                if (st.isEmpty()) {
                    return false;
                } else if (!isMatchingPair(st.pop(), exp.get(i))) {
                    return false;
                }
            }

        }

        if (st.isEmpty())
            return true;
        else {
            return false;
        }
    }
}
